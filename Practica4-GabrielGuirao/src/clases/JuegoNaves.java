package clases;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public class JuegoNaves {
	/**
	 * Arrays de las clases Nave y Hangar
	 */
	Nave[] naves; // array de la clase 1
	Hangar[] hangares; // array de la clase extra

	/**
	 * Constructor de la clase JuegoNaves
	 * 
	 * @param maxNaves
	 * @param maxBases
	 */
	public JuegoNaves(int maxNaves, int maxBases) {
		this.naves = new Nave[maxNaves];
		this.hangares = new Hangar[maxBases];

	}

	/**
	 * Alta de naves que no tienen escuadron, por defecto el tamano de esuadron es 1
	 * 
	 * @param nombre
	 * @param tamano
	 * @param coste
	 * @param escuadron
	 * @param faccion
	 */
	public void altaNaves(String nombre, int tamano, int coste, boolean escuadron, String faccion) {
		for (int i = 0; i < naves.length; i++) {
			boolean existeNave = false;
			for (int j = 0; j < Nave.getContadorNaves(); j++) {
				if (naves[j] != null) {
					if (naves[j].getNombre().equalsIgnoreCase(nombre)) {
						existeNave = true;
					}
				}
			}
			if (naves[i] == null) {
				naves[i] = new Nave(nombre, tamano, coste, escuadron, faccion);
				naves[i].setTamanoEscuadron(1);
				System.out.println("La nave con nombre " + naves[i].getNombre() + " se ha introducido correctamente");
				break;
			}
			if (existeNave) {
				System.out.println(
						"La nave no se ha creado correctamente ya que ya existe una nave registrada con ese nombre");
				break;
			}
		}
	}

	/**
	 * Constructor para las naves que si tienen un escuadron
	 * 
	 * @param nombre
	 * @param tamano
	 * @param coste
	 * @param escuadron
	 * @param faccion
	 * @param tamanoEscuadron
	 */
	public void altaNaves(String nombre, int tamano, int coste, boolean escuadron, String faccion,
			int tamanoEscuadron) {
		for (int i = 0; i < naves.length; i++) {
			boolean existeNave = false;
			for (int j = 0; j < Nave.getContadorNaves(); j++) {
				if (naves[j] != null) {
					if (naves[j].getNombre().equalsIgnoreCase(nombre)) {
						existeNave = true;
					}
				}
			}
			if (naves[i] == null) {
				naves[i] = new Nave(nombre, tamano, coste, escuadron, faccion, tamanoEscuadron);
				System.out.println("La nave con nombre " + naves[i].getNombre() + " se ha introducido correctamente");
				break;
			}
			if (existeNave) {
				System.out.println(
						"La nave no se ha creado correctamente ya que ya existe una nave registrada con ese nombre");
				break;
			}
		}
	}

	/**
	 * metodo para listar todas las naves, tambien muestra el total de naves
	 * registradas
	 */
	public void listadoNaves() {
		System.out.println("Hay un total de " + Nave.getContadorNaves() + " nave(s)");
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				System.out.println(naves[i]);
			}
		}
	}

	/**
	 * Metodo para buscar una nave por su nombre
	 * 
	 * @param nombre
	 * @return
	 */
	public Nave buscarNaves(String nombre) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombre)) {
					return naves[i];
				}
			}
		}

		return null;
	}

	/**
	 * Metodo con el que introduciendo elnombre de la nave te la muestra
	 * 
	 * @param nombre
	 * @return
	 */
	public boolean existeNave(String nombre) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombre)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Metodo para eliminar una nave introduciendo su nombre
	 * 
	 * @param nombre
	 */
	public void eliminarNaves(String nombre) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombre)) {
					naves[i] = null;
					System.out.println("La nave " + i + " se ha eliminado correctamente");
				}
			}
		}
	}

	/**
	 * Metodo para cambiar el nombre de la nave deseada
	 * 
	 * @param nombreNave
	 * @param nuevoNombre
	 */
	public void cambiarNombre(String nombreNave, String nuevoNombre) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombreNave)) {
					naves[i].setNombre(nuevoNombre);
					System.out
							.println("La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
					System.out.println(naves[i]);
				}
			}
		}
	}

	/**
	 * Metodo para cambiar el tamano de la nave
	 * 
	 * @param nombreNave
	 * @param nuevoTamano
	 */
	public void cambiarTamano(String nombreNave, int nuevoTamano) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombreNave)) {
					naves[i].setTamano(nuevoTamano);
					System.out
							.println("La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
					System.out.println(naves[i]);
				}
			}
		}
	}

	/**
	 * Metodo para cambiar el coste de la nave
	 * 
	 * @param nombreNave
	 * @param nuevoCoste
	 */
	public void cambiarCoste(String nombreNave, int nuevoCoste) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombreNave)) {
					naves[i].setTamano(nuevoCoste);
					System.out
							.println("La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
					System.out.println(naves[i]);
				}
			}
		}
	}

	/**
	 * Metodo para cambiar si la nave pertenece a algun escuadron o no si la nave
	 * pertenecia a un escuadron, el tamano del escuadron pasara a ser 1 si la nave
	 * no pertenecia a ningun escuadron, pedira por teclado el tamano del escuadron
	 * 
	 * @param nombreNave
	 * @param input
	 */
	public void cambiarEscuadron(String nombreNave, Scanner input) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombreNave)) {
					if (naves[i].isEscuadron() == true) {
						naves[i].setEscuadron(false);
						naves[i].setTamanoEscuadron(1);
						System.out.println(
								"La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
						System.out.println(naves[i]);
					} else {
						naves[i].setEscuadron(true);
						System.out.println("Introduce el tamano que tendra el escuedron");
						int nuevoTamanoEscuadron = input.nextInt();
						naves[i].setTamanoEscuadron(nuevoTamanoEscuadron);
						System.out.println(
								"La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
						System.out.println(naves[i]);
					}
				}
			}
		}
	}

	/**
	 * Metodo para cambiar la faccion a la que pertenece la nave
	 * 
	 * @param nombreNave
	 * @param nuevaFaccion
	 */
	public void cambiarFaccion(String nombreNave, String nuevaFaccion) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombreNave)) {
					naves[i].setFaccion(nuevaFaccion);
					System.out
							.println("La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
					System.out.println(naves[i]);
				}
			}
		}
	}
/**
 * Metodo para cambiar el tamano del escuadron
 * @param nombreNave
 * @param nuevoTamanoEscuadron
 */
	public void cambiarTamanoEscuadron(String nombreNave, int nuevoTamanoEscuadron) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getNombre().equalsIgnoreCase(nombreNave)) {
					naves[i].setTamanoEscuadron(nuevoTamanoEscuadron);
					System.out
							.println("La nave con nombre " + naves[i].getNombre() + " se ha modificado correctamente");
					System.out.println(naves[i]);
				}
			}
		}
	}
/**
 * Metodo para listar las naves que pertenecen a una faccion
 * @param faccion
 */
	public void listarPorFaccion(String faccion) {
		for (int i = 0; i < naves.length; i++) {
			if (naves[i] != null) {
				if (naves[i].getFaccion().equalsIgnoreCase(faccion)) {
					System.out.println(naves[i]);
				}
			}
		}
	}
/**
 * Metodo que te ordena las naves en funcion del precio
 */
	public void ordenarNavesPorCoste() {
		for (int i = 0; i < naves.length; i++) {
			for (int j = 0; j < naves.length; j++) {
				if (naves[i] != null && naves[j] != null) {
					if (naves[i].getCoste() < naves[j].getCoste()) {
						Nave aux = naves[i];
						naves[i] = naves[j];
						naves[j] = aux;
					}
				}
			}
		}
		System.out.println("Las naves se han ordenado correctamente");
	}

	// ********HANGARES***********
	/**
	 * Metodo para dar de alta los hangares
	 * @param nombre
	 * @param numero
	 */
	public void altaHangares(String nombre, int numero) {
		for (int i = 0; i < hangares.length; i++) {
			boolean existeBase = false;
			for (int j = 0; j < Hangar.getContadorHangares(); j++) {
				if (hangares[j] != null) {
					if (hangares[j].getNombre().equalsIgnoreCase(nombre)) {
						existeBase = true;
					}
				}
			}
			if (hangares[i] == null) {
				hangares[i] = new Hangar(nombre, numero);
				System.out
						.println("La base con nombre " + hangares[i].getNombre() + " se ha introducido correctamente");
				break;
			}
			if (existeBase) {
				System.out.println(
						"La base no se ha creado correctamente ya que ya existe una base registrada con ese nombre"
								+ hangares[i].getNombre());
				break;
			}
		}
	}
/**
 * Metodo para listar los hangares
 */
	public void listadoHangares() {
		System.out.println("Hay un total de " + Hangar.getContadorHangares() + " hangar(es)");
		for (int i = 0; i < hangares.length; i++) {
			if (hangares[i] != null) {
				System.out.println(hangares[i]);
			}
		}
	}

	/**
	 * Metodo para buscar hangares
	 * @param nombre
	 * @return
	 */
	public Hangar buscarHangares(String nombre) {
		for (int i = 0; i < hangares.length; i++) {
			if (hangares[i] != null) {
				if (hangares[i].getNombre().equalsIgnoreCase(nombre)) {
					return hangares[i];
				}
			}
		}

		return null;
	}

/**
 * Metodo para "aterrizar naves"
 * Este metodo une una nave a un hangar comprobando si la nave o el hangar existen y si el hangar tien ya una nave asignada
 * @param nombreNave
 * @param nombreHangar
 */
	public void aterrizarNave(String nombreNave, String nombreHangar) {
		boolean existeNave = false;
		boolean existeHangar = false;
		boolean hangarVacio = true;
		for (int i = 0; i < Hangar.getContadorHangares(); i++) {
			for (int j = 0; j < Nave.getContadorNaves(); j++) {
				if (hangares[i].getNombre().equalsIgnoreCase(nombreHangar)) {
					if (hangares[i].getNaves() != null) {
						hangarVacio = false;
					}
				}
				if (hangares[i].getNombre().equalsIgnoreCase(nombreHangar)) {
					existeHangar = true;
				}
				if (naves[j].getNombre().equalsIgnoreCase(nombreNave)) {
					existeNave = true;
				}
				if (existeNave && existeHangar && hangarVacio) {
					hangares[i].setNaves(naves[j]);
					hangares[i].setFechaUltimoEstacionamiento(LocalDate.now());
					System.out.println("Se ha aterrizado la nave con nombre " + naves[j].getNombre() + " en el hangar "+ hangares[i].getNombre());
					break;
				}
			}
		}

		if (existeNave == false) {
			System.out.println("La nave no esta registrada");
		}
		if (existeHangar == false) {
			System.out.println("El hangar no esta registrado");
		}
		if (hangarVacio == false) {
			System.out.println("El hangar ya contiene una nave");
		}
	}
}

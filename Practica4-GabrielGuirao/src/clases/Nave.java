package clases;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public class Nave {

	private String nombre;
	private int tamano;
	private int coste;
	private boolean escuadron;
	private int tamanoEscuadron;
	private String faccion;
	public static int contadorNaves = 0;

	/**
	 * Este constructor se utilizara en caso de de que la nave pertenezca a un
	 * escuadron, ya que tambine pide especificar el tamano del escuadron
	 * 
	 * @param nombre
	 * @param tamano
	 * @param coste
	 * @param escuadron
	 * @param faccion
	 * @param tamanoEscuadron
	 */

	public Nave(String nombre, int tamano, int coste, boolean escuadron, String faccion, int tamanoEscuadron) {
		this.nombre = nombre;
		this.tamano = tamano;
		this.coste = coste;
		this.escuadron = escuadron;
		this.faccion = faccion;
		this.tamanoEscuadron = tamanoEscuadron;
		contadorNaves++;
	}

	/**
	 * En caso de que la nave no pertenezca a un escuadron, se utilizara este
	 * constructor
	 */
	public Nave(String nombre, int tamano, int coste, boolean escuadron, String faccion) {
		this.nombre = nombre;
		this.tamano = tamano;
		this.coste = coste;
		this.escuadron = escuadron;
		this.faccion = faccion;
		contadorNaves++;
	}

	/**
	 * 
	 * @return devuelve el numero de naves registradas
	 */
	public static int getContadorNaves() {
		return contadorNaves;
	}

	/**
	 * 
	 * @return devuelve el nombre de la nave
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return devuelve el tamano de la nave
	 */
	public int getTamano() {
		return tamano;
	}

	public void setTamano(int tamano) {
		this.tamano = tamano;
	}

	/**
	 * 
	 * @return devuelve el coste de la nave
	 */
	public int getCoste() {
		return coste;
	}

	public void setCoste(int coste) {
		this.coste = coste;
	}

	/**
	 * 
	 * @return devuelve si la nave perteence a un escuadron o no
	 */
	public boolean isEscuadron() {
		return escuadron;
	}

	public void setEscuadron(boolean escuadron) {
		this.escuadron = escuadron;
	}

	/**
	 * 
	 * @return el tamano del escuadron
	 */
	public int getTamanoEscuadron() {
		return tamanoEscuadron;
	}

	public void setTamanoEscuadron(int tamanoEscuadron) {
		this.tamanoEscuadron = tamanoEscuadron;
	}

	/**
	 * 
	 * @return la faccion a la que pertenece la nave
	 */
	public String getFaccion() {
		return faccion;
	}

	public void setFaccion(String faccion) {
		this.faccion = faccion;
	}

	/**
	 * ToString
	 */
	@Override
	public String toString() {
		return "Nave [nombre=" + nombre + ", tamano=" + tamano + ", coste=" + coste + ", escuadron=" + escuadron
				+ ", tamanoEscuadron=" + tamanoEscuadron + ", faccion=" + faccion + "]";
	}

}

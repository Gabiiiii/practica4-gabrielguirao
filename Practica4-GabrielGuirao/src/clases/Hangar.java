package clases;

import java.time.LocalDate;

/**
 * 
 * @author Gabriel Guirao
 *
 */
public class Hangar {
	private String nombre;
	private int numero;
	private LocalDate fechaUltimoEstacionamiento;
	private Nave nave;
	public static int contadorHangares = 0;

	/**
	 * Esta clase solo tiene un constructor con los siguientes parametros
	 * 
	 * @param nombre
	 * @param numero
	 */
	public Hangar(String nombre, int numero) {
		super();
		this.nombre = nombre;
		this.numero = numero;
		contadorHangares++;
	}

	/**
	 * 
	 * @return devuelve el nombre del hangar
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return devuelve el numero del hangar
	 */
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * 
	 * @return devuelve la Nave aterrizada en el hangar
	 */
	public Nave getNaves() {
		return nave;
	}

	public void setNaves(Nave naves) {
		this.nave = naves;
	}

	/**
	 * 
	 * @return devuelve la cantidad de hangares registrados
	 */
	public static int getContadorHangares() {
		return contadorHangares;
	}

	/**
	 * 
	 * @return devuelve la fecha en la que se ha estacionado la ultima nave
	 */
	public LocalDate getFechaUltimoEstacionamiento() {
		return fechaUltimoEstacionamiento;
	}

	public void setFechaUltimoEstacionamiento(LocalDate fechaUltimoEstacionamiento) {
		this.fechaUltimoEstacionamiento = fechaUltimoEstacionamiento;
	}

	@Override
	public String toString() {
		return "Hangar [Nombre=" + nombre + ", Numero=" + numero + ", FechaUltimoEstacionamiento="
				+ fechaUltimoEstacionamiento + "]" + "\nNave Estacionada=" + nave;
	}

}

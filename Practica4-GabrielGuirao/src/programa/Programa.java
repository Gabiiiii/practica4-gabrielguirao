package programa;

import java.util.Scanner;

import clases.JuegoNaves;
/**
 * 
 * @author Gabriel Guirao
 *
 */
public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
/**
 * asignacion de los maximos de los arrays de Nave y Hangar
 */
		System.out.println("Introduce el maximo de naves a crear");
		int maxNaves = input.nextInt();
		System.out.println("Ahora introduce el maximo de bases a crear");
		int maxBases = input.nextInt();
		/**
		 * Alta inicial de algunas naves y hangares
		 * y colocacion de una nave en su hangar
		 */
		JuegoNaves juegoNaves = new JuegoNaves(maxNaves, maxBases);
		juegoNaves.altaNaves("Fragata", 45, 100, false, "Imperio");
		juegoNaves.altaNaves("Fragata", 56, 100, true, "Imperio", 4);
		juegoNaves.altaNaves("X-wing", 56, 50, true, "Imperio", 7);
		juegoNaves.altaNaves("Nave del emperador Mariano Rajoy", 45, 10000, true, "PP", 10);
		juegoNaves.altaHangares("Hangar del PP", 5);
		juegoNaves.aterrizarNave("Nave del emperador Mariano Rajoy", "Hangar del PP");
		juegoNaves.altaHangares("Hangar1", 15);
		juegoNaves.altaHangares("Hangar2", 14);
		juegoNaves.altaHangares("Hangar3", 4);
		boolean salir = false;
		do {
			System.out.println("_____________________________________");
			System.out.println("1- Dar de alta naves                 |");
			System.out.println("2- Listar naves                      |");
			System.out.println("3- Buscar naves                      |");
			System.out.println("4- ELiminar naves                    |");
			System.out.println("5- Cambiar atributo de una nave      |");
			System.out.println("6- Listar naves de una faccion       |");
			System.out.println("7- Ordenar naves por coste           |");
			System.out.println("8- Dar de alta un hangar             |");
			System.out.println("9- Listar hangares                   |");
			System.out.println("10- Buscar hangares                  |");
			System.out.println("11- Aterrizar nave en un hangar      |");
			System.out.println("12- Salir                            |");
			System.out.println("_____________________________________|");
			int menu = input.nextInt();
			input.nextLine();
			switch (menu) {
			case 1:
				System.out.println("Introduce el nombre de la nave");
				String nombre = input.nextLine();
				System.out.println("Introduce el tamano de la nave");
				int tamano = input.nextInt();
				System.out.println("Introduce el coste de la nave");
				int coste = input.nextInt();
				input.nextLine();
				System.out.println("�A que faccion pertenece la nave?");
				String faccion = input.nextLine();
				System.out.println("�La nave pertence a un escuadron? (si/no)");
				String escuadron = input.nextLine();
				if (escuadron.equalsIgnoreCase("SI")) {
					System.out.println("Introduce el tamanio del escuadron");
					int tamanioEscuadron = input.nextInt();
					juegoNaves.altaNaves(nombre, tamano, coste, true, faccion, tamanioEscuadron);
				} else if (escuadron.equalsIgnoreCase("No")) {
					juegoNaves.altaNaves(nombre, tamano, coste, false, faccion);
				}
				break;
			case 2:
				juegoNaves.listadoNaves();
				break;
			case 3:
				System.out.println("Introduce el nombre de la nave que quieres buscar");
				String buscarNave = input.nextLine();
				System.out.println(juegoNaves.buscarNaves(buscarNave));
				break;
			case 4:
				System.out.println("Introduce el nombre de la nave que quieres eliminar");
				String eliminarNave = input.nextLine();
				juegoNaves.eliminarNaves(eliminarNave);
				break;
			case 5:
				System.out.println("Introduce el nombre de la nave que quieres modificar");
				String nombreNave = input.nextLine();
				if (juegoNaves.existeNave(nombreNave) == true) {
					System.out.println("Introduce el nombre del artibuto que quieres cambiar");
					System.out
							.println("Atributos a cambiar: nombre, tamano, coste, escuadron, tamanoEscuadron, faccion");
					String nuevoAtributo = input.nextLine();
					nuevoAtributo = nuevoAtributo.toLowerCase();
					switch (nuevoAtributo) {
					case "nombre":
						System.out.println("Introduce el nuevo nombre de la nave");
						String nuevoNombre = input.nextLine();
						juegoNaves.cambiarNombre(nombreNave, nuevoNombre);
						break;
					case "tamano":
						System.out.println("Introduce el nuevo tamano de la nave");
						int nuevoTamano = input.nextInt();
						juegoNaves.cambiarTamano(nombreNave, nuevoTamano);
						break;
					case "coste":
						System.out.println("Introduce el nuevo coste de la nave");
						int nuevoCoste = input.nextInt();
						juegoNaves.cambiarCoste(nombreNave, nuevoCoste);
						break;
					case "escuadron":
						juegoNaves.cambiarEscuadron(nombreNave, input);
						break;
					case "faccion":
						System.out.println("Introduce la nueva faccion de la nave");
						String nuevaFaccion = input.nextLine();
						juegoNaves.cambiarFaccion(nombreNave, nuevaFaccion);
						break;
					case "tamanoEscuadron":
						System.out.println("Introduce el nuevo tamano del escuadron de la nave");
						int nuevoTamanoEscuadron = input.nextInt();
						juegoNaves.cambiarTamanoEscuadron(nombreNave, nuevoTamanoEscuadron);
						break;
					}
				} else {
					System.out.println("La nave introducida no esta registrada");
				}
				break;
			case 6:
				System.out.println("Introduce el nombre de la faccion de la que quieres ver las naves");
				String listarFaccion=input.nextLine();
				juegoNaves.listarPorFaccion(listarFaccion);
				break;
			case 7:
				juegoNaves.ordenarNavesPorCoste();
				juegoNaves.listadoNaves();
				break;
			case 8:
				break;
			case 9:
				juegoNaves.listadoHangares();
				break;
			case 10:
				System.out.println("Introduce el nombre de la base que quieres buscar");
				String buscarHangar = input.nextLine();
				System.out.println(juegoNaves.buscarHangares(buscarHangar));
				break;
			case 11:
				System.out.println("Introduce el nombre de la nave que quieres aterrizar ");
				String naveAterrizar=input.nextLine();
				System.out.println("Introduce el nombre del hangar");
				String hangarAterrizar=input.nextLine();
				juegoNaves.aterrizarNave(naveAterrizar, hangarAterrizar);
				break;
			case 12:
				salir = true;
				System.out.println("Fin del programa");
				break;
			default:
				System.out.println("Elige una opcion valida");
			}
		} while (!salir);

		input.close();
	}

}
